//
//  CheapFlightsTests.swift
//  CheapFlightsTests
//
//  Created by pwnstarSmithy on 15/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import XCTest
@testable import Cheap_Flights

class CheapFlightsTests: XCTestCase {
    
    func testSquareInt() {
        
        let value = 3
        
        let processedValue = value.square()
        
        XCTAssertEqual(processedValue, 9)
        
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
