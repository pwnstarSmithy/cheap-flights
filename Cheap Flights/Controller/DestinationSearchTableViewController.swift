//
//  DestinationSearchTableViewController.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 11/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

//because this is an objectiveC lib, this will give error "Cannot load underlying module for SwiftSpinner" but once you build it, it finds it.
import SwiftSpinner

class DestinationSearchTableViewController: UITableViewController, UISearchBarDelegate {

    var searchResults = [AirportsByCities]()
    

    
    @IBOutlet weak var searchBar: UISearchBar!
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    //DO something when searbar cancel button is clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.view.endEditing(true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        
    }
    
    //do certain things when search bar search button has been clicked!
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
    SwiftSpinner.show("Rendering Icebergs to simulate global warming!")
        
        if searchBar.text != nil {
            
            globalDestination = searchBar.text
            
            let keywords = searchBar.text
            let cleanKeywords = keywords?.replacingOccurrences(of: " ", with: "+")
            
            guard let url = URL(string: "http://iatacodes.org/api/v6/autocomplete?api_key=\(IataCodeApiKey)&query=\(cleanKeywords!)")else{return}
            
            //request url
            var request = URLRequest(url: url as URL)
            
            request.httpMethod = "POST"
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: request) { (data, response, error) in
                
                guard let data = data else {return}
                
                do {
                    
                    let searched = try JSONDecoder().decode(Cities.self, from: data)
                    
                    let searchResponse = searched.response!
                    
                    let airportsArray = searchResponse.airports_by_cities!
             
                    SwiftSpinner.hide()
                    
                    for finalArray in airportsArray{
                        
                        DispatchQueue.main.async(execute: {
                            
                            self.searchResults.append(finalArray)
                            self.tableView.reloadData()
                        })
                        
                 
                        
                    }
                    
                }catch{
                    
                    SwiftSpinner.hide()
                    
                    print(error)
                }
            }
            
            DispatchQueue.main.async(execute: {
                
                self.view.endEditing(true)
            })
            
            task.resume()
            
        }else{
            let failAlert = UIAlertController(title: "Error", message: "Please enter a city into the search bar so we can help you", preferredStyle: UIAlertControllerStyle.alert)
            
            failAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { (action) in
                
                failAlert.dismiss(animated: true, completion: nil)
                
                
            }))
            
            DispatchQueue.main.async(execute: {
                self.present(failAlert, animated: true, completion: nil)
            })
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return searchResults.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "destinationCityCell", for: indexPath) as! DestinationSearchResultsTableViewCell
        
        cell.airportCode.text = searchResults[indexPath.row].code
        cell.airportName.text = searchResults[indexPath.row].name
        cell.countryName.text = searchResults[indexPath.row].country_name

        // Configure the cell...

        return cell
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        destinationCode = searchResults[indexPath.row].code
        
        //segue to first screen.
 self.navigationController?.popViewController(animated: true)
        
    }
    

}
