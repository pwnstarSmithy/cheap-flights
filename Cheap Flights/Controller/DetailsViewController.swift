//
//  DetailsViewController.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 12/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBAction func flightSwitch(_ sender: UISwitch) {
        
        if (sender.isOn == true){
            
            flightType = 1
            
        }else{
            flightType = 0
            
        }
        
    }
    

    @IBAction func doneWithDetails(_ sender: Any) {
        
        //format data
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let dateString = formatter.string(from: datePicker.date)
        
        
        flightDate = dateString
        
 self.navigationController?.popViewController(animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        flightType = 1
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
