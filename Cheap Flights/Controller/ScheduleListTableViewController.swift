//
//  ScheduleListTableViewController.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 12/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class ScheduleListTableViewController: UITableViewController {
    var flightSchedule : ScheduleDetails?
    var flightArray = [ScheduleArray]()
    
    var departAirport : DepartureDetails?
    var arrivalAirport : ArrivalDetails?
    
    var mainArray = [moreFLight]()
    var journeyString : JourneyDetails?
    var flightDate : dateDeparture?
    var flightName : String?
    
    //variables to get airport longitude and latitude
    var airportResource : airportData?
    var airports : moreAirport?
    var airport : singleAirport?
    var airportPosition : positionDetails?
    var airportCoordinate : cordinateDetails?
    
    
    var airportLatitude : Double?
    var airportLongitude : Double?
    
    var airportLatitudeOrigin : Double?
    var airportLongitudeOrigin : Double?
    
    var airportLatitudeDestination : Double?
    var airportLongitudeDestination : Double?
    
    
    //variables to hold destination and origin
    var originVar : String?
    var destinationVar : String?
    
    var airportCode : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        //call functions to get position details
        
        getOriginPosition()
        getDestinationPosition()
        
        //append flight array to empty array flightarray
        
        
        for ama in (flightSchedule?.Schedule)! {



            flightArray.append(ama)



        }
    }
    
   
    
    func getOriginPosition() {
        //empty these variables coz they need to be empty in function to get airports
        airportLatitude = nil
        airportLongitude = nil
        
        //assign airport code based on this function
        airportCode = originCode
        
        //assign origin airport to airport code
        self.getAirportDetails()
        
    }
    
    func getDestinationPosition() {
        //empty these variables coz they are needed in function to get airports
        airportLatitude = nil
        airportLongitude = nil
        
        //assign airport code based on this function
        airportCode = destinationCode

        //assign origin airport to airport code
        self.getAirportDetails()
        
    }
    
    func getAirportDetails() {
        
        let url = NSURL(string: "https://api.lufthansa.com/v1/references/airports/\(airportCode!)")!
        
        let tokenString = "Bearer " + accessToken!
        
        var request = URLRequest(url: url as URL)
        
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
         
            if error == nil {
                
                do {
                    
                    let pulledAirport = try JSONDecoder().decode(AirportInformation.self, from: data)
                    
                    self.airportResource = pulledAirport.AirportResource
                    
                    self.airports = self.airportResource?.Airports
                    
                    self.airport = self.airports?.Airport
                    
                    self.airportPosition = self.airport?.Position
                    
                    self.airportCoordinate = self.airportPosition?.Coordinate
                    
                    self.airportLatitude = self.airportCoordinate?.Latitude
                    self.airportLongitude = self.airportCoordinate?.Longitude
                    
                    
                    
                    //run checks to see if airport latitude for either destination or origin are empty then assign accordingly!
                    
                    if self.airportLatitudeOrigin == nil {
                        
                        //assign latitude and logitude specific to origin airport to specific variables
                        self.airportLatitudeOrigin = self.airportLatitude
                        self.airportLongitudeOrigin = self.airportLongitude
                     
                        
                    }else {
                        
                        //assign latitude and logitude specific to origin airport to specific variables
                        self.airportLatitudeDestination = self.airportLatitude
                        self.airportLongitudeDestination = self.airportLongitude
                        
                    }
                    
                    
                }catch{
                    
                    print(error)
                    
                }
                
                
            }else{
                
                print(error!)
                
            }
            
            }.resume()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return flightArray.count
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scheduleCell", for: indexPath) as! ScheduleTableViewCell
        
        cell.flightName.text = "\(globalOrigin!) - \(globalDestination!)"
        
        if flightArray[indexPath.row].Flight.isEmpty {
            
 
        }else{
            
            mainArray = flightArray[indexPath.row].Flight
            
            journeyString = flightArray[indexPath.row].TotalJourney
  
            
            for dummyArray in mainArray {
                
                departAirport = dummyArray.Departure
                arrivalAirport = dummyArray.Arrival
       
                flightDate = departAirport?.ScheduledTimeLocal
                
            }
            
        }
        
        //functions to remove characters from the time variable until it has format we want to present to user.
        let durationDummy = journeyString?.Duration
        
        let swiftyTime = durationDummy?.replacingOccurrences(of: "H", with: " Hours ")
        
        let minuteTime = swiftyTime?.replacingOccurrences(of: "M", with: " Minutes.")
        
        let minusPT = minuteTime?.replacingOccurrences(of: "PT", with: "")
        
        let minusP = minusPT?.replacingOccurrences(of: "P", with: "")
        
        let minusD = minusP?.replacingOccurrences(of: "D", with: " Day ")
        
        cell.flightDuration.text = minusD
        
        cell.flightStops.text = "\(mainArray.count) Airport Stops"
        
        // configure the date returned into a desired format!
        let dateString = flightDate?.DateTime
        
        let dateSwify = dateString?.replacingOccurrences(of: "T", with: " at ")
        
        cell.flightDate.text = dateSwify

        // Configure the cell...

        return cell
    }
 

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? MapViewController {
            
            
            destination.airportLatitudeOrigin = airportLatitudeOrigin
            destination.airportLongitudeOrigin = airportLongitudeOrigin
            
            
            destination.airportLatitudeDestination = airportLatitudeDestination
            destination.airportLongitudeDestination = airportLongitudeDestination
            
            
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }


}
