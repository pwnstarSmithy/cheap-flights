//
//  DestinationSearchResultsTableViewCell.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 11/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class DestinationSearchResultsTableViewCell: UITableViewCell {

    @IBOutlet weak var airportCode: UILabel!
    
    @IBOutlet weak var airportName: UILabel!
    
    @IBOutlet weak var countryName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
