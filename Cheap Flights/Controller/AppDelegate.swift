//
//  AppDelegate.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 11/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

//global token to be accessed from all views in the app
var accessToken : String?

var originCode : String?
var destinationCode : String?

var flightType : Int?
var flightDate : String?

var globalOrigin : String?
var globalDestination : String?

var IataCodeApiKey = "41fdccc5-7693-4e64-8d1f-8784f715cb17"

// global variable refered to appDelegate to be able to call it from any class / file.swift
let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    
    var window: UIWindow?
    
//    func firstScreen() {
//
//        // refer to our Main.storyboard
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        // store our tabBar Object from Main.storyboard in tabBar var
//        let homeTabBar = storyboard.instantiateViewController(withIdentifier: "firstMap")
//
//        let navigationController = UINavigationController(rootViewController: homeTabBar)
//
//
//
//        // present tabBar that is stored in navigation controller
//        window?.rootViewController = navigationController
//
//
////        let storyboard : UIStoryboard = UIStoryboard(name: "AccountStoryboard", bundle: nil)
////        let vc : WelcomeViewController = storyboard.instantiateViewController(withIdentifier: "WelcomeID") as! WelcomeViewController
////        vc.teststring = "hello"
////
////        let navigationController = UINavigationController(rootViewController: vc)
////
////        self.present(navigationController, animated: true, completion: nil)
//
//    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}
