//
//  MapViewController.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 12/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit


import GoogleMaps
import GooglePlaces

class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    //prepare for values passed from previous view
    var airportLatitudeOrigin : Double?
    var airportLongitudeOrigin : Double?
    
    var airportLatitudeDestination : Double?
    var airportLongitudeDestination : Double?
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //request for authorization from user to use location
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()

        
        
        if airportLongitudeOrigin != nil && airportLatitudeOrigin != nil && airportLatitudeDestination != nil && airportLongitudeDestination != nil {
            
                    loadMaps()
            
        }else{
            
            //present alertview to user notifying him/her the car isn't on the map.
            let failAlert = UIAlertController(title: "Error", message: "We don't have location details for this particular flight", preferredStyle: UIAlertControllerStyle.alert)
            
            failAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { (action) in
                
                failAlert.dismiss(animated: true, completion: nil)
                
                
            }))
            
            DispatchQueue.main.async(execute: {
                self.present(failAlert, animated: true, completion: nil)
            })
            
            
        }

        // Do any additional setup after loading the view.
    }
    
    func loadMaps(){


        //provide the google maps api key that allows the app to connect to the service.
        GMSServices.provideAPIKey("AIzaSyCoj89BF85hLGn640PeX3F2lI7huya_tTc")
        
        let path = GMSMutablePath()
        path.add(CLLocationCoordinate2D(latitude: airportLatitudeOrigin!, longitude: airportLongitudeOrigin!))
        path.add(CLLocationCoordinate2D(latitude: airportLatitudeDestination!, longitude: airportLongitudeDestination!))
        

//        let marker = GMSMarker()
//        marker.icon = UIImage(named: "airport")
//
        let mapView = GMSMapView(frame: self.view.bounds)
        view = mapView
        
        let rectangle = GMSPolyline(path: path)
        rectangle.map = mapView
        

        //show markers on map
        let originPosition = CLLocationCoordinate2D(latitude: airportLatitudeOrigin!, longitude: airportLongitudeOrigin!)
        let originMarker = GMSMarker(position: originPosition)
        originMarker.title = "Origin Airport"
        originMarker.icon = UIImage(named: "airport")
        originMarker.map = mapView
        
        let destinationPosition = CLLocationCoordinate2D(latitude: airportLatitudeDestination!, longitude: airportLongitudeDestination!)
        let destinationMarker = GMSMarker(position: destinationPosition)
        destinationMarker.title = "Destination Airport"
        destinationMarker.icon = UIImage(named: "airport")
        destinationMarker.map = mapView
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
