//
//  OriginResultsTableViewCell.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 11/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class OriginResultsTableViewCell: UITableViewCell {

    @IBOutlet weak var AirportCode: UILabel!
    
    @IBOutlet weak var AirportName: UILabel!
    
    @IBOutlet weak var AirportCountry: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
