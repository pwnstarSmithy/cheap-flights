//
//  ViewController.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 11/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import MapKit

//because this is an objectiveC lib, this will give error "Cannot load underlying module for SwiftSpinner" but once you build it, it finds it.
import SwiftSpinner


var schedulePulled : ScheduleDetails?

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    
    //assign parameters to be used later for access token call
    var clientID = "mmfd68tzenrma8ay6hv44uhp"
    var clientSecret = "gnyVsSG9Md"
    var grantType = "client_credentials"
    
    @IBOutlet weak var firstMap: MKMapView!
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var originSearch: UITextField!
    
    @IBOutlet weak var destinationSearch: UITextField!
    
    @IBAction func searchPressed(_ sender: Any) {
  
        //assign default value if flight type not set
        if flightType == nil {
            
            flightType = 0
        }
   

        //assign default date value if not set
        if flightDate == nil {
            let date = Date()
            let calendar = Calendar.current
            
            let day = calendar.component(.day, from: date)
            let month = calendar.component(.month, from: date)
            let year = calendar.component(.year, from: date)
 
            
            let currentDate = "\(year)-0\(month)-\(day)"
            
            flightDate = currentDate
            
        }
        
        if (originSearch.text?.isEmpty)! && (destinationSearch.text?.isEmpty)! {
            
            let failAlert = UIAlertController(title: "Error", message: "Please make sure you have edited flight details and have entered origin and destination", preferredStyle: UIAlertControllerStyle.alert)
            
            failAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { (action) in
                
                failAlert.dismiss(animated: true, completion: nil)
                
                
            }))
            
            DispatchQueue.main.async(execute: {
                self.present(failAlert, animated: true, completion: nil)
            })
        }else{
             searchAirports()
        }

        
    }

 
    func searchAirports(){
        
        //start animation using SwiftSpinner
        SwiftSpinner.show("firing up engines, say goodbye to earth")
        
        let url = NSURL(string: "https://api.lufthansa.com/v1/operations/schedules/\(originCode!)/\(destinationCode!)/\(flightDate!)?directFlights=\(flightType!)")!
    
        let tokenString = "Bearer " + accessToken!
        
        var request = URLRequest(url: url as URL)
        
        //set authorization parameter in authorization header
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            //check for any errors
            
            if error == nil {
                
                do {
                    
                    let scheduleDetails = try JSONDecoder().decode(Schedules.self, from: data)
                    
                    
                    schedulePulled = scheduleDetails.ScheduleResource
                    
        
                    SwiftSpinner.hide()
                    let successAlert = UIAlertController(title: "Flight Schedules", message: "We've successfully found some matching schedules based on your selections!", preferredStyle: UIAlertControllerStyle.alert)
                    
                    successAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                        
                        self.performSegue(withIdentifier: "toTable", sender: nil)
                        
                        
                    }))
                    
                    DispatchQueue.main.async(execute: {
                        self.present(successAlert, animated: true, completion: nil)
                    })
                    
                    
                }catch{
                    SwiftSpinner.hide()
                    let failAlert = UIAlertController(title: "Error", message: "Failed to find any direct flights, please try again with different selections or try flights with stops", preferredStyle: UIAlertControllerStyle.alert)
                    
                    failAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { (action) in
                        
                        failAlert.dismiss(animated: true, completion: nil)
                        
                        
                    }))
                    
                    DispatchQueue.main.async(execute: {
                        self.present(failAlert, animated: true, completion: nil)
                    })
                    
                }
                
                
            }else{
           
                SwiftSpinner.hide()
                let failAlert = UIAlertController(title: "Error", message: "Failed to find any schedules, please try again with different selections", preferredStyle: UIAlertControllerStyle.alert)
                
                failAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { (action) in
                    
                    failAlert.dismiss(animated: true, completion: nil)
                    
                    
                }))
                
                DispatchQueue.main.async(execute: {
                    self.present(failAlert, animated: true, completion: nil)
                })
                
            }
            
            }.resume()
        
        
        
        
    }
    
    
    func getAccessToken(){
        
        let url = NSURL(string: "https://api.lufthansa.com/v1/oauth/token")!
        
        var request = URLRequest(url: url as URL)
        
        request.httpMethod = "POST"
        let body = "client_id=\(clientID)&client_secret=\(clientSecret)&grant_type=\(grantType)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)

        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else{return}
            // check if no error
            if error == nil{
                
                do{
                
                    let tokenDetails = try JSONDecoder().decode(Token.self, from: data)
                    
                    accessToken = tokenDetails.access_token
                    
                    //save generated token to UserDefaults, this saves it onto users phone
                    UserDefaults.standard.set(accessToken, forKey: "Token")
                    
                    
                }catch{
                   
                    
                }
            }else{
                return
            }
            }.resume()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillTextfields()
        getAccessToken()
        
        loadMaps()
  
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fillTextfields()
        loadMaps()
    }
    
    func loadMaps() {
    
        firstMap.delegate = self
   
        firstMap.showsPointsOfInterest = true
        firstMap.showsUserLocation = true
        firstMap.showsBuildings = true
        firstMap.mapType = .standard
        firstMap.isZoomEnabled = true
        firstMap.isScrollEnabled = true

    
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
      
        }
  

        //Zoom to user location
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation, 200, 200)

            firstMap.setRegion(viewRegion, animated: true)
        }

    }
    
    func fillTextfields() {
        if originCode != nil {
            
            originSearch.text = originCode
            
        }else{
            
            
        }
        if destinationCode != nil {
            
            destinationSearch.text = destinationCode
            
        }else{
            
        }
        
    }
    
    @IBAction func originTextFieldTapped(_ sender: Any) {
        
          performSegue(withIdentifier: "originSearch", sender: self)
    }
    
    
    @IBAction func destinationTextFieldTapped(_ sender: Any) {
        performSegue(withIdentifier: "destinationView", sender: self)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? ScheduleListTableViewController {
            
            destination.flightSchedule = schedulePulled
            
            //send destination and origin variables to next screen
            destination.originVar = originCode
            destination.destinationVar = destinationCode
 
            
            let flightNames = "\(originCode!) - \(destinationCode!)"
            
            destination.flightName = flightNames
        }
    
    }
}
extension Int {
    
    func square() -> Int {
        return self * self
    }
    
}

