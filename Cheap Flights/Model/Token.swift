//
//  Token.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 12/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

struct Token : Decodable {
    let access_token : String
    let token_type : String
    let expires_in : Int
}
