//
//  Schedules.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 12/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
struct Schedules : Decodable{
    let ScheduleResource : ScheduleDetails
}

struct ScheduleDetails : Decodable {
    let Schedule : [ScheduleArray]
}

struct ScheduleArray : Decodable  {
    
    let TotalJourney : JourneyDetails
    let Flight : [moreFLight]
    
}
struct  JourneyDetails: Decodable{
    let Duration : String?
}


struct moreFLight : Decodable {
    let Departure : DepartureDetails?
    let Arrival : ArrivalDetails?
    let MarketingCarrier : MarketingDetails?
    let OperatingCarrier : OperatingDetails?
    let Details : DetailsDic?
}

struct DepartureDetails : Decodable {
    let AirportCode : String?
    let ScheduledTimeLocal : dateDeparture?
}

struct dateDeparture : Decodable {
    let DateTime : String?
}

struct ArrivalDetails : Decodable {
    let AirportCode : String?
    let ScheduledTimeLocal : dateArrival?
}

struct  dateArrival : Decodable{
    let DateTime : String?
}


struct MarketingDetails : Decodable {
    let AirlineID : String?
    let FlightNumber : Int?
}

struct OperatingDetails : Decodable {
    let AirlineID : String?
}


struct DetailsDic : Decodable {
    let Stops : StopsList?
    let DaysOfOperation : Int?
    let DatePeriod : DatePeriodDetails?
}

struct StopsList : Decodable {
    let StopQuantity : Int?
}

struct DatePeriodDetails : Decodable {
    let Effective : String?
    let Expiration : String?
}


