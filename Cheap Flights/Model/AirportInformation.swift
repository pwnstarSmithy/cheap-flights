//
//  AirportDetails.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 12/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
struct AirportInformation : Decodable {
    
    let AirportResource : airportData?
    
}

struct airportData : Decodable {
    let Airports : moreAirport?
}

struct moreAirport : Decodable {
    let Airport : singleAirport?
}

struct singleAirport : Decodable {
    let AirportCode : String?
    let Position : positionDetails?
}

struct positionDetails : Decodable {
    let Coordinate : cordinateDetails?
}

struct cordinateDetails : Decodable {
    let Latitude : Double?
    let Longitude : Double?
}
