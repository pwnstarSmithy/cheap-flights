//
//  Cities.swift
//  Cheap Flights
//
//  Created by pwnstarSmithy on 11/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

struct Cities : Decodable {
    let response : ResponseDic?
}

struct ResponseDic : Decodable {
    
    let airports_by_cities : [AirportsByCities]?
    
}

struct AirportsByCities : Decodable {
    let code : String?
    let name : String?
    let country_name : String?
}
