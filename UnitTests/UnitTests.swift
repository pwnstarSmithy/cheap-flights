//
//  UnitTests.swift
//  UnitTests
//
//  Created by pwnstarSmithy on 16/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import XCTest
//build it so it can create a link to the test target, then error will disappear
@testable import Cheap_Flights

class UnitTests: XCTestCase {
    
    func testInt(){
        let value = 3
        
        let squaredValue = value.square()
        
        XCTAssertEqual(squaredValue, 9)
        
    }
    
    func testIataCode(){
        
        guard let iataUrl = URL(string: "http://iatacodes.org/api/v6/autocomplete?api_key=\(IataCodeApiKey)&query=Entebbe") else { return }
        
        let promise = expectation(description: "Simple Request")
 
        //request url
        var request = URLRequest(url: iataUrl as URL)
        
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {return}
            
            do {
                
                let searched = try JSONDecoder().decode(Cities.self, from: data)
                
                let searchResponse = searched.response!
                
                let airportsArray = searchResponse.airports_by_cities!
                
                for finalArray in airportsArray{
                  
                        XCTAssertTrue(finalArray.code == "EBB")
                    
                }
                promise.fulfill()
            }catch{
                
                print(error)
            }
        }
        
  
        
        task.resume()
        waitForExpectations(timeout: 10, handler: nil)
    }

    func testGetAccessToken(){
        
        //assign parameters to be used later for access token call
        let clientID = "mmfd68tzenrma8ay6hv44uhp"
        let clientSecret = "gnyVsSG9Md"
        let grantType = "client_credentials"
        
        //send request to server
        let url = NSURL(string: "https://api.lufthansa.com/v1/oauth/token")!
        
          let promise = expectation(description: "Simple Request")
        
        //request url
        var request = URLRequest(url: url as URL)
        
        // method to pass data
        request.httpMethod = "POST"
        let body = "client_id=\(clientID)&client_secret=\(clientSecret)&grant_type=\(grantType)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        //launch session
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else{return}
            // check if no error
            if error == nil{
                
                do{
                    
                    let tokenDetails = try JSONDecoder().decode(Token.self, from: data)
                    
                    accessToken = tokenDetails.access_token
                    
                    let tokenType = tokenDetails.token_type
                    
                    XCTAssertTrue(tokenType == "bearer")
                    
                  promise.fulfill()
                    
                }catch{
                    
                    
                }
            }else{
                return
            }
            }.resume()
        waitForExpectations(timeout: 10, handler: nil)
        
    }
    
    func testGetAirportDetails(){
        
        
        //url to send the request to
        let url = NSURL(string: "https://api.lufthansa.com/v1/references/airports/EBB")!
         let promise = expectation(description: "Simple Request")
        //append access token to bearer string
        let tokenString = "Bearer " + accessToken!
        
        //request for url
        var request = URLRequest(url: url as URL)
        
        //set authorization parameter in authorization header
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
                do {
                    
                    let pulledAirport = try JSONDecoder().decode(AirportInformation.self, from: data)


            let airportResource = pulledAirport.AirportResource
            let airports = airportResource?.Airports
            let airport = airports?.Airport
                    
             let airportCode = airport?.AirportCode
                    
                    XCTAssertTrue(airportCode == "EBB")
                    
                    promise.fulfill()
                }catch{
                
                    
                }
            
            }.resume()
         waitForExpectations(timeout: 10, handler: nil)
    }

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
